#pragma once

#include <cstddef>
#include <iostream>
#include <memory>

using namespace std;

/**
 * An abstract base class for all figure solvers.
 */
class FigureSolver
{
private:
    size_t width;       /**< Matrix width. */
    size_t height;      /**< Matrix height. */

public:
    /**
     * Default constructor.
     *
     * Initializes an empty matrix with zero-length dimensions.
     */
    FigureSolver():
        width(0),
        height(0)
    { }

    /**
     * Fixed width and height constructor.
     *
     * Initializes width and height of the matrix.
     * No memory allocation at this point.
     *
     * @param width Matrix width.
     * @param height Matrix height.
     */
    FigureSolver(size_t width, size_t height):
        width(width),
        height(height)
    { }

    /**
     * Memory initialization.
     *
     * Called from constructors of inheriting classes
     * and from I/O >> operator to allocate memory after
     * #width and #height has been set.
     */
    virtual void init() = 0;

    inline size_t GetWidth() const { return width; }                /**< #width getter. */
    inline size_t GetHeight() const { return height; }              /**< #height getter. */
    inline size_t MatrixSize() const { return width * height; }     /**< Calculates total matrix size. */

    virtual void SetField(size_t x, size_t y) = 0;                  /**< Marks a field in the matrix. */
    virtual void UnsetField(size_t x, size_t y) = 0;                /**< Unmark a field in the matrix. */
    virtual uint8_t GetField(size_t x, size_t y) const = 0;         /**< Returns a field in the matrix (0 or 1). */

    /**
     * Finds the number of figures in the matrix.
     *
     * @return Number of closed figures in the matrix.
     */
    virtual size_t Solve() = 0;

    /**
     * Memory size calculator.
     *
     * Calculates the matrix memory size after #width and #height has been set.
     *
     * @return Memory size in bytes.
     */
    virtual size_t MemorySize() const = 0;

    /**
     * \a istream >> operator overload.
     *
     * Overloads >> operator for \a istream and \a FigureSolver.
     * Loads object data from a stream.
     *
     * @param is Input stream.
     * @param solver \a FigureSolver object.
     */
    friend istream& operator >>(istream& is, FigureSolver& solver);
};

/** \relates FigureSolver
 * \a ostream << operator overload.
 *
 * Overloads << operator for \a ostream and \a FigureSolver.
 * Saves object data to a stream.
 *
 * @param os Output stream.
 * @param solver \a FigureSolver object.
 */
ostream& operator <<(ostream& os, FigureSolver const& solver);

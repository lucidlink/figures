#include <stdexcept>
#include <stack>

#include "MemoryOptimizedFigureSolver.hpp"


void MemoryOptimizedFigureSolver::init()
{
    /* Allocate array memory or set to nullptr if amount is zero */
    arrayLenInBytes = MemorySize();
    if (arrayLenInBytes)
        array = make_unique<uint8_t[]>(arrayLenInBytes);
    else
        array = nullptr;
}

inline void MemoryOptimizedFigureSolver::BitPosition(size_t x, size_t y, size_t& bytePos, size_t& bitPos) const
{
    /* Validate position */
    if (x < 0 || x >= GetWidth())
        throw out_of_range("X is out of range");
    if (y < 0 || y >= GetHeight())
        throw out_of_range("Y is out of range");

    /* Calculate byte and bit position */
    size_t pos = y * GetWidth() + x;
    bytePos = pos / 8;
    bitPos = pos % 8;
}

void MemoryOptimizedFigureSolver::SetField(size_t x, size_t y)
{
    size_t bytePos, bitPos;

    /* Get byte and bit position, set element */
    BitPosition(x, y, bytePos, bitPos);
    array[bytePos] |= ((uint8_t)0x01 << bitPos);
}

void MemoryOptimizedFigureSolver::UnsetField(size_t x, size_t y)
{
    size_t bytePos, bitPos;

    /* Get byte and bit position, unset element */
    BitPosition(x, y, bytePos, bitPos);
    array[bytePos] &= ~((uint8_t)0x01 << bitPos);
}

uint8_t MemoryOptimizedFigureSolver::GetField(size_t x, size_t y) const
{
    size_t bytePos, bitPos;

    /* Get byte and bit position, return element */
    BitPosition(x, y, bytePos, bitPos);
    return array[bytePos] & ((uint8_t)0x01 << bitPos);
}

/*
 * Find figures using flood-fill algorithm.
 */
size_t MemoryOptimizedFigureSolver::Solve()
{
    size_t x, y;
    size_t figuresNumber = 0;
    stack<Position> positionStack;
    Position pos;

    /* Traverse array by X and Y */
    for (y=0; y<GetHeight(); y++) {
        for (x=0; x<GetWidth(); x++) {

            /* Continue if current element is not marked */
            if (!FastGetField(x, y))
                continue;

            /* A figure begins, increase counter, add first element into the stack */
            figuresNumber++;
            positionStack.push((Position){x, y});

            /* While elements in the stack... */
            while (!positionStack.empty()) {
                /* Get next element */
                pos = positionStack.top();
                positionStack.pop();

                /*
                 * Unmark element.
                 * Check up, down, left, and right for marked elements.
                 * Add all marked elements into the stack.
                 */
                FastUnsetField(pos.x, pos.y);
                if (pos.x > 0 && FastGetField(pos.x - 1, pos.y))
                    positionStack.push((Position){pos.x - 1, pos.y});
                if (pos.x < GetWidth() - 1 && FastGetField(pos.x + 1, pos.y))
                    positionStack.push((Position){pos.x + 1, pos.y});
                if (pos.y > 0 && FastGetField(pos.x, pos.y - 1))
                    positionStack.push((Position){pos.x, pos.y - 1});
                if (pos.y < GetHeight() - 1 && FastGetField(pos.x, pos.y + 1))
                    positionStack.push((Position){pos.x, pos.y + 1});
            }
        }
    }

    return figuresNumber;
}

size_t MemoryOptimizedFigureSolver::MemorySize() const
{
    /*
     * Calculate the amount of bytes needed to store all elements.
     * One bit per element.
     */
    return (MatrixSize() > 0 ? (MatrixSize() - 1) / 8 + 1 : 0);
}

# Figures

## Assignment

#### The problem
Having a matrix of **n** by **m** elements. Each of them can be **marked or not**. In the
example included in ```5x5.matrix``` sample file this is expressed by 0s (not marked) and
1s (marked). The goal is to find the **number of all individual figures**, by one figure we mean all
marked elements that touch each other by one of their sides. If two cells touch only a vertex
(diagonally) and not a common side, they are not considered one figure.

#### Requirements
* Used programming language - C++.
* It is important that the code is written professionally as a commercial software **meeting
all quality requirements as such**. It would be good to consider a scalable solution that
would work with large matrices of millions of elements.
* It is of no use to anyone if the algorithm is taken from the Internet, but on the other hand,
it is not forbidden to seek knowledge in the problem area if you think it is necessary.

## Release notes

The goal of this project is to implement and optimize the algorithm for finding figures in a matrix. Parts of the project are not fully optimized.

For example, I/O operations could be further optimized for a specific job. As an example, matrix files could be binary, which would increase the speed, but affect the ability to manually edit them.

Scalability is limited to the maximum available memory. It could also be optimized by using additional disk drive memory or horizontal scaling, which will also affect performance, and the choice depends on the parameters of the particular job.

Ultimately, I chose this approach because of the nature of the test assignment, so that it could be as user-friendly and open to editing as possible, with a maximum focus on the optimization of shape-finding algorithms.

## Prerequisites

1. Linux system.
1. GNU C++ compiler (```g++```) supporting C++14 standard.
1. GNU ```make``` tool.

## 3rd party libraries

None

## Build

1. Type ```make```

Two binary files are produced.

* ```genfig``` - used to generate randomly initialized matrices of different sizes.
* ```solver``` - used to find the number of figures in a matrix previously saved in a file.

## Usage examples

#### Generator
```
Usage: ./genfig <width> <height> <elements> [<file>]
Generate matrix with given width and height and randomly marked elements.
        width            - matrix width
        height           - matrix height
        elements         - number of randomly marked elements
        file             - output file (or STDOUT if not specified)
```

_Note: Random function could override previously marked elements._

* Generate 5x5 matrix with 10 randomly marked elements similar to example from assignment and save it to ```sample.matrix```. 
  ```
  ./genfig 5 5 10 sample.matrix
  ```

* Generate large 50,000 by 50,000 matrix of 2.5 billion elements with 1.25 billion marked elements and save it to ```large.matrix```.
  ```
  ./genfig 50000 50000 1250000000 large.matrix
  ```

#### Solver
```
Usage: ./solver <file> <algorithm>
Find number of figures in a matrix.
        file             - input file
        algorithm        - optimization algorithm ("speed" or "memory")
```
* Solve ```5x5.matrix``` with speed optimized algorithm (file included in this release)
  ```
  $ ./solver 5x5.matrix speed
  Loading from 5x5.matrix...
  File loaded in 0.000042889 seconds
  Searching for figures...
  Matrix size is 5x5 elements (25 in total)
  Algorithm is optimized for speed
  Found 3 figures
  Solved in 0.000003566 seconds
  Used 25 bytes of memory for matrix data
  ```
  The algorithm finds 3 figures as expected.
* Solve ```sample.matrix``` with both algorithms
  * Speed optimized
  ```
  $ ./solver sample.matrix speed
  Loading from sample.matrix...
  File loaded in 0.000050507 seconds
  Searching for figures...
  Matrix size is 5x5 elements (25 in total)
  Algorithm is optimized for speed
  Found 5 figures
  Solved in 0.000002506 seconds
  Used 25 bytes of memory for matrix data
  ```
  * Memory optimized
  ```
  $ ./solver sample.matrix memory
  Loading from sample.matrix...
  File loaded in 0.000050708 seconds
  Searching for figures...
  Matrix size is 5x5 elements (25 in total)
  Algorithm is optimized for memory
  Found 5 figures
  Solved in 0.000003959 seconds
  Used 4 bytes of memory for matrix data
  ```
  The difference in times for small matrices is within the error caused by timer resolution and OS multitasking.
  The difference in memory, on the other hand, is obvious. 25 bytes for speed optimized algorithm vs. 4 bytes for memory optimized algorithm.

* Solve ```large.matrix``` with both algorithms
  * Speed optimized
  ```
  $ ./solver large.matrix speed
  Loading from large.matrix...
  File loaded in 17.686679687 seconds
  Searching for figures...
  Matrix size is 50000x50000 elements (2500000000 in total)
  Algorithm is optimized for speed
  Found 270693547 figures
  Solved in 25.863371971 seconds
  Used 2500000000 bytes of memory for matrix data
  ```
  * Memory optimized
  ```
  $ ./solver large.matrix memory
  Loading from large.matrix...
  File loaded in 18.926399394 seconds
  Searching for figures...
  Matrix size is 50000x50000 elements (2500000000 in total)
  Algorithm is optimized for memory
  Found 270693547 figures
  Solved in 29.505929648 seconds
  Used 312500000 bytes of memory for matrix data
  ```
  This time the time difference is obvious, approx. 3.643s. The difference in the amount of memory used is, as you might expect, 8 times again. 2.33 Gb for speed optimized algorithm vs. 298 Mb for memory optimized algorithm.

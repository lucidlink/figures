#include <cstdlib>
#include <fstream>
#include <cstring>

#include "MemoryOptimizedFigureSolver.hpp"
#include "SpeedOptimizedFigureSolver.hpp"
#include "Chronometer.hpp"


/**
 * Loads a matrix from a file and finds the number of figures.
 * Prints out the results.
 *
 * @param[out] input_file Name of the input file.
 * @param[out] algorithm Optimization algorithm - "speed" or "memory".
 */
void solve(string& input_file, string& algorithm);

/**
 * Parses command line arguments.
 *
 * @param argc \a argc from \a main() function.
 * @param argv \a argv from \a main() function.
 * @param[out] input_file Name of the input file.
 * @param[out] algorithm Optimization algorithm - "speed" or "memory".
 */
void cmdparse(int argc, char **argv, string& input_file, string& algorithm);

/**
 * Prints out usage information and terminates the program.
 *
 * @param prgname Name of the program, \a argv[0].
 * @param exitcode Exit code for \a exit() function.
 */
void usage(const char *prgname, int exitcode);


int main(int argc, char **argv)
{
    string input_file, algorithm;

    /* Set cout precision for times */
    cout.precision(9);

    /* Parse command line arguments */
    cmdparse(argc, argv, input_file, algorithm);

    try {
        /* Find and print out the number of figures */
        solve(input_file, algorithm);
    }
    catch (ofstream::failure const&) {
        cout << "ERROR: Failed to write to output file" << endl;
        return -3;
    }
    catch (bad_alloc const&) {
        cout << "ERROR: Failed to allocate memory" << endl;
        return -1;
    }
    catch (out_of_range const& e) {
        cout << "ERROR: Out of range error: " << e.what() << endl;
        return -2;
    }
    catch (runtime_error const& e) {
        cout << "ERROR: Format error: " << e.what() << endl;
        return -3;
    }

    return 0;
}

void solve(string& input_file, string& algorithm)
{
    unique_ptr<FigureSolver> matrix;
    Chronometer chronometer;

    /* Create an object according to the chosen algorithm */
    if (algorithm == "speed")
        matrix = make_unique<SpeedOptimizedFigureSolver>();
    else
        matrix = make_unique<MemoryOptimizedFigureSolver>();

    /* Load from file, measure and print out the duration */
    cout << "Loading from " << input_file << "..." << endl;

    chronometer.run();
    ifstream is;
    is.exceptions(ifstream::badbit | ifstream::failbit);
    is.open(input_file);
    is >> *matrix;
    is.close();
    chronometer.stop();

    cout << fixed << "File loaded in " << chronometer.duration() << " seconds" << endl;
    cout << "Searching for figures..." << endl;

    /* Find figures, measure the duration */
    chronometer.run();
    size_t figuresNumber = matrix->Solve();
    chronometer.stop();

    /* Print out useful information */
    cout << "Matrix size is " <<
            matrix->GetWidth() << "x" << matrix->GetHeight() << " elements " <<
            "(" << matrix->MatrixSize() << " in total)" << endl;
    cout << "Algorithm is optimized for " << algorithm << endl;
    cout << "Found " << figuresNumber << " figures" << endl;
    cout << fixed << "Solved in " << chronometer.duration() << " seconds" << endl;
    cout << "Used " << matrix->MemorySize() << " bytes of memory for matrix data" << endl;
}


void cmdparse(int argc, char **argv, string& input_file, string &algorithm)
{
    /* Check if number of arguments is correct */
    if (argc < 3)
        usage(argv[0], 0);

    /* Parse two arguments - input file and algorithm */
    input_file = argv[1];
    algorithm = argv[2];

    /* Validate algorithm */
    if (algorithm != "speed" && algorithm != "memory")
        usage(argv[0], -1);
}

void usage(const char *prgname, int exitcode)
{
    cout << "Usage: " << prgname << " " <<
        "<file> <algorithm>" << endl;
    cout << "Find number of figures in a matrix." << endl;
    cout << "\tfile\t\t - input file" << endl;
    cout << "\talgorithm\t - optimization algorithm (\"speed\" or \"memory\")" << endl;
    exit(exitcode);
}

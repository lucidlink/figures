
BIN=genfig solver
OBJECTS=FigureSolver.o MemoryOptimizedFigureSolver.o SpeedOptimizedFigureSolver.o
BINOBJECTS=$(BIN:=.o)
HEADERS=$(OBJECTS:.o=.hpp)
CC=g++
CFLAGS=-Wall -O2 -std=c++14

.PHONY: all clean

all: $(BIN)

$(BIN): %: %.o $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) $@.o -o $@

$(OBJECTS): %.o: %.cpp %.hpp
	$(CC) -c $(CFLAGS) $< -o $@

%.o: %.cpp $(HEADERS)
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -f $(BINOBJECTS) $(OBJECTS) $(BIN)

#pragma once

#include "FigureSolver.hpp"

/**
 * A figure solver optimized for speed.
 *
 * Saves each element in a byte.
 * Preserves the data in the array, so it can be restored and reused after a search.
 */
class SpeedOptimizedFigureSolver:
    public FigureSolver
{
private:
    size_t arrayLenInBytes;         /**< Length of array in bytes. */
    unique_ptr<uint8_t[]> array;    /**< Matrix array. */

    /**
     * Element position.
     *
     * Helper struct for flood fill algorithm.
     */
    struct Position {
        size_t x, y;
    };

    /**
     * Fast unset element.
     *
     * An inline helper function to unset element.
     * Doesn't validate \a x and \a y.
     */
    inline void FastUnsetField(size_t x, size_t y) {
        size_t pos = y * GetWidth() + x;
        array[pos] |= 0x80;
    }

    /**
     * Fast get element.
     *
     * An inline helper function to get element.
     * Doesn't validate \a x and \a y.
     */
    inline uint8_t FastGetField(size_t x, size_t y) const {
        size_t pos = y * GetWidth() + x;
        return array[pos];
    }

public:
    /**
     * Default constructor.
     *
     * Initializes an empty matrix with zero-length dimensions.
     * Doesn't allocate memory.
     */
    SpeedOptimizedFigureSolver():
        FigureSolver()
    {
        init();
    }

    /**
     * Fixed width and height constructor.
     *
     * Initializes width and height of the matrix.
     * Allocates memory for the matrix.
     *
     * @param width Matrix width.
     * @param height Matrix height.
     */
    SpeedOptimizedFigureSolver(size_t width, size_t height):
        FigureSolver(width, height)
    {
        init();
    }

    /**
     * Memory initialization.
     *
     * Called from constructors and from I/O >> operator
     * to allocate memory after #width and #height has been set.
     */
    virtual void init();

    /**
     * Byte position calculator.
     *
     * Calculates the byte position of element.
     *
     * @param x X position of the element.
     * @param y Y position of the element.
     * @return bytePos Element's byte position in array.
     */
    inline size_t BytePosition(size_t x, size_t y) const;

    virtual void SetField(size_t x, size_t y);                  /**< Marks a field in the matrix. */
    virtual void UnsetField(size_t x, size_t y);                /**< Unmark a field in the matrix. */
    virtual uint8_t GetField(size_t x, size_t y) const;         /**< Returns a field in the matrix (0 or 1). */

    /**
     * Finds the number of figures in the matrix.
     *
     * @return Number of closed figures in the matrix.
     */
    virtual size_t Solve();

    /**
     * Memory size calculator.
     *
     * Calculates the matrix memory size after #width and #height has been set.
     *
     * @return Memory size in bytes.
     */
    virtual size_t MemorySize() const;
};

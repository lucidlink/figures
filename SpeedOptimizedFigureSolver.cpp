#include <stdexcept>
#include <stack>

#include "SpeedOptimizedFigureSolver.hpp"


void SpeedOptimizedFigureSolver::init()
{
    /* Allocate array memory or set to nullptr if amount is zero */
    arrayLenInBytes = MemorySize();
    if (arrayLenInBytes)
        array = make_unique<uint8_t[]>(arrayLenInBytes);
    else
        array = nullptr;
}

inline size_t SpeedOptimizedFigureSolver::BytePosition(size_t x, size_t y) const
{
    /* Validate position */
    if (x < 0 || x >= GetWidth())
        throw out_of_range("X is out of range");
    if (y < 0 || y >= GetHeight())
        throw out_of_range("Y is out of range");

    /* Calculate byte position */
    return (y * GetWidth() + x);
}

void SpeedOptimizedFigureSolver::SetField(size_t x, size_t y)
{
    /* Get byte position, set element */
    size_t pos = BytePosition(x, y);
    array[pos] = 1;
}

void SpeedOptimizedFigureSolver::UnsetField(size_t x, size_t y)
{
    /* Get byte position, unset element */
    size_t pos = BytePosition(x, y);
    array[pos] = 0;
}

uint8_t SpeedOptimizedFigureSolver::GetField(size_t x, size_t y) const
{
    /* Get byte position, return element */
    size_t pos = BytePosition(x, y);
    return array[pos];
}

/*
 * Find figures using flood-fill algorithm.
 */
size_t SpeedOptimizedFigureSolver::Solve()
{
    size_t x, y;
    size_t figuresNumber = 0;
    stack<Position> positionStack;
    Position pos;

    /* Traverse array by X and Y */
    for (y=0; y<GetHeight(); y++) {
        for (x=0; x<GetWidth(); x++) {

            /* Continue if current element is not marked */
            if (FastGetField(x, y) != 1)
                continue;

            /* A figure begins, increase counter, add first element into the stack */
            figuresNumber++;
            positionStack.push((Position){x, y});

            /* While elements in the stack... */
            while (!positionStack.empty()) {
                /* Get next element */
                pos = positionStack.top();
                positionStack.pop();

                /*
                 * Unmark element.
                 * Check up, down, left, and right for marked elements.
                 * Add all marked elements into the stack.
                 */
                FastUnsetField(pos.x, pos.y);
                if (pos.x > 0 && FastGetField(pos.x - 1, pos.y) == 1)
                    positionStack.push((Position){pos.x - 1, pos.y});
                if (pos.x < GetWidth() - 1 && FastGetField(pos.x + 1, pos.y) == 1)
                    positionStack.push((Position){pos.x + 1, pos.y});
                if (pos.y > 0 && FastGetField(pos.x, pos.y - 1) == 1)
                    positionStack.push((Position){pos.x, pos.y - 1});
                if (pos.y < GetHeight() - 1 && FastGetField(pos.x, pos.y + 1) == 1)
                    positionStack.push((Position){pos.x, pos.y + 1});
            }
        }
    }

    return figuresNumber;
}

size_t SpeedOptimizedFigureSolver::MemorySize() const
{
    /*
     * Calculate the amount of bytes needed to store all elements.
     * One byte per element.
     */
    return MatrixSize();
}

#include <ctime>
#include <cstdlib>
#include <fstream>
#include <cstring>

#include "MemoryOptimizedFigureSolver.hpp"


/**
 * Creates matrix and saves it to a file.
 *
 * @param width Matrix width.
 * @param height Matrix height.
 * @param elements Number of randomly marked elements.
 * @param output_file Name of the output file or empty string for STDOUT.
 */
void generate(size_t width, size_t height, size_t elements, string output_file);

/**
 * Parses command line arguments.
 *
 * @param argc \a argc from \a main() function.
 * @param argv \a argv from \a main() function.
 * @param[out] width Matrix width.
 * @param[out] height Matrix height.
 * @param[out] elements Number of randomly marked elements.
 * @param[out] output_file Name of the output file or empty string.
 */
void cmdparse(int argc, char **argv,
        size_t& width, size_t& height, size_t& elements, string& output_file);

/**
 * Prints out usage information and terminates the program.
 *
 * @param prgname Name of the program, \a argv[0].
 * @param exitcode Exit code for \a exit() function.
 */
void usage(const char *prgname, int exitcode);


int main(int argc, char **argv)
{
    size_t width, height, elements;
    string output_file;

    /* Initialize random seed */
    srand(time(0));

    try {
        /* Parse command line arguments */
        cmdparse(argc, argv, width, height, elements, output_file);

        /* Generate and save matrix */
        generate(width, height, elements, output_file);
    }
    catch (ofstream::failure const&) {
        cout << "ERROR: Failed to write to output file" << endl;
        return -3;
    }
    catch (bad_alloc const&) {
        cout << "ERROR: Failed to allocate memory" << endl;
        return -1;
    }
    catch (out_of_range const& e) {
        cout << "ERROR: Out of range error: " << e.what() << endl;
        return -2;
    }
    catch (runtime_error const& e) {
        cout << "ERROR: Format error: " << e.what() << endl;
        return -3;
    }

    return 0;
}

void generate(size_t width, size_t height, size_t elements, string output_file)
{
    /*
     * It is assumed that during generation memory is more important than speed
     * in order to be able to generate largest amounts of data and so
     * MemoryOptimizedFigureSolver is chosen to generate matrix.
     */
    MemoryOptimizedFigureSolver matrix(width, height);

    /* Randomly mark elements */
    for (size_t i=0; i<elements; i++) {
        size_t x = rand() % width;
        size_t y = rand() % height;
        matrix.SetField(x, y);
    }

    /* Output data to a file... */
    if (output_file.length()) {
        ofstream os;
        os.exceptions(ofstream::badbit | ofstream::failbit);
        os.open(output_file);
        os << matrix;
        os.close();
    }

    /* or to STDOUT */
    else
        cout << matrix;
}

void cmdparse(int argc, char **argv,
        size_t& width, size_t& height, size_t& elements, string& output_file)
{
    /* Check if number of arguments is correct */
    if (argc < 4)
        usage(argv[0], 0);

    /* Parse three or four arguments - width, height, elements, and output file */
    if (sscanf(argv[1], "%zu", &width) != 1)
        throw runtime_error("Invalid width");
    if (sscanf(argv[2], "%zu", &height) != 1)
        throw runtime_error("Invalid height");
    if (sscanf(argv[3], "%zu", &elements) != 1)
        throw runtime_error("Invalid number of elements");
    if (argc >= 5)
        output_file = argv[4];
}

void usage(const char *prgname, int exitcode)
{
    cout << "Usage: " << prgname << " " <<
        "<width> <height> <elements> [<file>]" << endl;
    cout << "Generate matrix with given width and height and randomly marked elements." << endl;
    cout << "\twidth\t\t - matrix width" << endl;
    cout << "\theight\t\t - matrix height" << endl;
    cout << "\telements\t - number of randomly marked elements" << endl;
    cout << "\tfile\t\t - output file (or STDOUT if not specified)" << endl;
    exit(exitcode);
}

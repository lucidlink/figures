var hierarchy =
[
    [ "Chronometer", "classChronometer.html", null ],
    [ "FigureSolver", "classFigureSolver.html", [
      [ "MemoryOptimizedFigureSolver", "classMemoryOptimizedFigureSolver.html", null ],
      [ "SpeedOptimizedFigureSolver", "classSpeedOptimizedFigureSolver.html", null ]
    ] ],
    [ "MemoryOptimizedFigureSolver::Position", "structMemoryOptimizedFigureSolver_1_1Position.html", null ],
    [ "SpeedOptimizedFigureSolver::Position", "structSpeedOptimizedFigureSolver_1_1Position.html", null ]
];
var files_dup =
[
    [ "Chronometer.hpp", "Chronometer_8hpp.html", [
      [ "Chronometer", "classChronometer.html", "classChronometer" ]
    ] ],
    [ "FigureSolver.cpp", "FigureSolver_8cpp.html", "FigureSolver_8cpp" ],
    [ "FigureSolver.hpp", "FigureSolver_8hpp.html", [
      [ "FigureSolver", "classFigureSolver.html", "classFigureSolver" ]
    ] ],
    [ "genfig.cpp", "genfig_8cpp.html", "genfig_8cpp" ],
    [ "MemoryOptimizedFigureSolver.cpp", "MemoryOptimizedFigureSolver_8cpp.html", null ],
    [ "MemoryOptimizedFigureSolver.hpp", "MemoryOptimizedFigureSolver_8hpp.html", [
      [ "MemoryOptimizedFigureSolver", "classMemoryOptimizedFigureSolver.html", "classMemoryOptimizedFigureSolver" ],
      [ "Position", "structMemoryOptimizedFigureSolver_1_1Position.html", "structMemoryOptimizedFigureSolver_1_1Position" ]
    ] ],
    [ "solver.cpp", "solver_8cpp.html", "solver_8cpp" ],
    [ "SpeedOptimizedFigureSolver.cpp", "SpeedOptimizedFigureSolver_8cpp.html", null ],
    [ "SpeedOptimizedFigureSolver.hpp", "SpeedOptimizedFigureSolver_8hpp.html", [
      [ "SpeedOptimizedFigureSolver", "classSpeedOptimizedFigureSolver.html", "classSpeedOptimizedFigureSolver" ],
      [ "Position", "structSpeedOptimizedFigureSolver_1_1Position.html", "structSpeedOptimizedFigureSolver_1_1Position" ]
    ] ]
];
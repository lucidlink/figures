var classFigureSolver =
[
    [ "FigureSolver", "classFigureSolver.html#a6c8aab43b61adba23e38f7d978a30cfd", null ],
    [ "FigureSolver", "classFigureSolver.html#ab5b6dd81981426122c3174c9fc173208", null ],
    [ "GetField", "classFigureSolver.html#a9f2ffcf6f44d3e563acffafd688eec53", null ],
    [ "GetHeight", "classFigureSolver.html#a3c0a088c14f542219bf55c107fa8956e", null ],
    [ "GetWidth", "classFigureSolver.html#abf11fa329b209c1cc321f17f7b3c9364", null ],
    [ "init", "classFigureSolver.html#a170aadfa625f897943aa42bffb6bad07", null ],
    [ "MatrixSize", "classFigureSolver.html#ae73437a8dc40298c3d71f131b12e6b43", null ],
    [ "MemorySize", "classFigureSolver.html#af55ca6bb28abb26fbf61265b9a345fd5", null ],
    [ "SetField", "classFigureSolver.html#a86884e5552d18fecc67c5169859c9b4f", null ],
    [ "Solve", "classFigureSolver.html#a9314e3d8669d419468ead02c2b9d09f3", null ],
    [ "UnsetField", "classFigureSolver.html#af7ec309d53907ad5d478cc2433d77e66", null ],
    [ "operator<<", "classFigureSolver.html#a3f648e1e37e05dce72fff971984ac235", null ],
    [ "operator>>", "classFigureSolver.html#a8ce76e039a83d5d5168c49594a14df49", null ],
    [ "height", "classFigureSolver.html#a0ca9f8a9cc0d26aa49db9668ce7ed223", null ],
    [ "width", "classFigureSolver.html#a2a9a47a33ab2dc8ef33d52b11e7801b5", null ]
];
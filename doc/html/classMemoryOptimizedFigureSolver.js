var classMemoryOptimizedFigureSolver =
[
    [ "Position", "structMemoryOptimizedFigureSolver_1_1Position.html", "structMemoryOptimizedFigureSolver_1_1Position" ],
    [ "MemoryOptimizedFigureSolver", "classMemoryOptimizedFigureSolver.html#afd47fe036784c566611f4fae9e85d792", null ],
    [ "MemoryOptimizedFigureSolver", "classMemoryOptimizedFigureSolver.html#a67a61b7c03be9f4611dfd0d11c84aec5", null ],
    [ "BitPosition", "classMemoryOptimizedFigureSolver.html#ad68a7fa8261c5421a9a3331e099865e1", null ],
    [ "FastGetField", "classMemoryOptimizedFigureSolver.html#aff185bdd899ffca12f9ff1790d8e3a05", null ],
    [ "FastUnsetField", "classMemoryOptimizedFigureSolver.html#af78257681b4f96bafd5107f083027010", null ],
    [ "GetField", "classMemoryOptimizedFigureSolver.html#a048e672938469347bee3994b75d8f754", null ],
    [ "init", "classMemoryOptimizedFigureSolver.html#ac0be7589c631275033b6da6d585fd80a", null ],
    [ "MemorySize", "classMemoryOptimizedFigureSolver.html#ac287fc1f860011371525fa6fa2ee1672", null ],
    [ "SetField", "classMemoryOptimizedFigureSolver.html#a7b58446caeadbb7e5d5a70527af2ca87", null ],
    [ "Solve", "classMemoryOptimizedFigureSolver.html#a33a2d3ee9ba27d5d51d604af9fc05ced", null ],
    [ "UnsetField", "classMemoryOptimizedFigureSolver.html#a4d4d1bc974137d39abca5c97a00d9c53", null ],
    [ "array", "classMemoryOptimizedFigureSolver.html#abb01e413a10e5322cba7e3eab38f2194", null ],
    [ "arrayLenInBytes", "classMemoryOptimizedFigureSolver.html#a652bc39d1ad96bca330959bd19ce38dd", null ]
];
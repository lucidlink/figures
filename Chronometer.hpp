#pragma once

#include <chrono>

using namespace std;

/**
 * A chronometer class.
 *
 * Measures the duration of operations.
 */
class Chronometer
{
private:
    chrono::steady_clock::time_point
        start,      /**< Operation start time. */
        end;        /**< Operation end time. */

public:
    /**
     * Saves operation start time.
     */
    inline void run()
        { start = chrono::steady_clock::now(); }

    /**
     * Saves operation end time.
     */
    inline void stop()
        { end = chrono::steady_clock::now(); }

    /**
     * Calculates duration of operation.
     *
     * @return Operation time in seconds.
     */
    inline double duration() const
    {
        double time_span =
            (double)chrono::duration_cast<chrono::nanoseconds>(end - start).count();

        return time_span / 1000000000.0;
    }
};

#pragma once

#include "FigureSolver.hpp"

/**
 * A figure solver optimized for memory.
 *
 * Saves each element in a single bit.
 * To save memory it destroys the the data in the array while searching for figures,
 * thus can be used to solve loaded array only once.
 */
class MemoryOptimizedFigureSolver:
    public FigureSolver
{
private:
    size_t arrayLenInBytes;         /**< Length of array in bytes. */
    unique_ptr<uint8_t[]> array;    /**< Matrix array. */

    /**
     * Element position.
     *
     * Helper struct for flood fill algorithm.
     */
    struct Position {
        size_t x, y;
    };

    /**
     * Fast unset element.
     *
     * An inline helper function to unset element.
     * Doesn't validate \a x and \a y.
     */
    inline void FastUnsetField(size_t x, size_t y) {
        size_t pos = y * GetWidth() + x;
        array[(pos >> 3)] &= ~((uint8_t)0x01 << (pos & 0x07));
    }

    /**
     * Fast get element.
     *
     * An inline helper function to get element.
     * Doesn't validate \a x and \a y.
     */
    inline uint8_t FastGetField(size_t x, size_t y) const {
        size_t pos = y * GetWidth() + x;
        return array[(pos >> 3)] & ((uint8_t)0x01 << (pos & 0x07));
    }

public:
    /**
     * Default constructor.
     *
     * Initializes an empty matrix with zero-length dimensions.
     * Doesn't allocate memory.
     */
    MemoryOptimizedFigureSolver():
        FigureSolver()
    {
        init();
    }

    /**
     * Fixed width and height constructor.
     *
     * Initializes width and height of the matrix.
     * Allocates memory for the matrix.
     *
     * @param width Matrix width.
     * @param height Matrix height.
     */
    MemoryOptimizedFigureSolver(size_t width, size_t height):
        FigureSolver(width, height)
    {
        init();
    }

    /**
     * Memory initialization.
     *
     * Called from constructors and from I/O >> operator
     * to allocate memory after #width and #height has been set.
     */
    virtual void init();

    /**
     * Bit position calculator.
     *
     * Calculates the byte and bit position of element.
     *
     * @param x X position of the element.
     * @param y Y position of the element.
     * @param[out] bytePos Element's byte position in array.
     * @param[out] bitPos Element's bit position in byte.
     */
    inline void BitPosition(size_t x, size_t y, size_t& bytePos, size_t& bitPos) const;

    virtual void SetField(size_t x, size_t y);                  /**< Marks a field in the matrix. */
    virtual void UnsetField(size_t x, size_t y);                /**< Unmark a field in the matrix. */
    virtual uint8_t GetField(size_t x, size_t y) const;         /**< Returns a field in the matrix (0 or 1). */

    /**
     * Finds the number of figures in the matrix.
     *
     * @return Number of closed figures in the matrix.
     */
    virtual size_t Solve();

    /**
     * Memory size calculator.
     *
     * Calculates the matrix memory size after #width and #height has been set.
     *
     * @return Memory size in bytes.
     */
    virtual size_t MemorySize() const;
};

#include <exception>

#include "FigureSolver.hpp"

istream& operator >>(istream& is, FigureSolver& solver)
{
    size_t x, y;
    string line;

    /* Read and validate width and height */
    is >> solver.width;
    if (!is || solver.width < 1)
        throw out_of_range("Width is out of range");

    is >> solver.height;
    if (!is || solver.height < 1)
        throw out_of_range("Height is out of range");

    /* Initialize matrix memory */
    solver.init();

    /* Skip the rest of the first line */
    getline(is, line);

    /* Read data line by line and symbol by symbol */
    for (y=0; y<solver.height; y++) {
        getline(is, line);
        for (x=0; x<solver.width; x++) {
            if (line[x] == '0')
                solver.UnsetField(x, y);
            else if (line[x] == '1')
                solver.SetField(x, y);
            else
                throw runtime_error("Malformed figure initialization: " + line);
        }
    }

    return is;
}

ostream& operator <<(ostream& os, FigureSolver const& solver)
{
    size_t x, y;

    /* Save width and height */
    os << solver.GetWidth() << ' ' << solver.GetHeight() << endl;

    /* Save elements, HEIGHT lines by WIDTH elements per line */
    for (y=0; y<solver.GetHeight(); y++) {
        for (x=0; x<solver.GetWidth(); x++) {
            os << (solver.GetField(x, y) ? '1' : '0');
        }
        os << endl;
    }

    return os;
}
